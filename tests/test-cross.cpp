#include <cassert>
#include <cstdlib>
#include <spirv.hpp>
#include <spirv_common.hpp>
#include <stdio.h>
#include <stdint.h>

#include <iostream>
#include <iterator>
#include <utility>
#include <iostream>
#include <fstream>
#include <vector>
#include <spirv_cross.hpp>
#include <spirv_reflect.hpp>

namespace spvc = spirv_cross;

static const char *BTMap[] = {
    [spvc::SPIRType::Unknown] = "unknown",
    [spvc::SPIRType::Void] = "void",
    [spvc::SPIRType::Boolean] = "Boolean",
    [spvc::SPIRType::SByte] = "Signed byte",
    [spvc::SPIRType::UByte] = "Unsigned byte",
    [spvc::SPIRType::Short] = "short",
    [spvc::SPIRType::UShort] = "unsigned short",
    [spvc::SPIRType::Int] = "int",
    [spvc::SPIRType::UInt] = "uint",
    [spvc::SPIRType::Int64] = "int64_t",
    [spvc::SPIRType::UInt64] = "uint64_t",
    [spvc::SPIRType::AtomicCounter] = "atomic counter",
    [spvc::SPIRType::Half] = "half float",
    [spvc::SPIRType::Float] = "float",
    [spvc::SPIRType::Double] = "Double",
    [spvc::SPIRType::Struct] = "struct",
    [spvc::SPIRType::Image] = "image",
    [spvc::SPIRType::SampledImage] = "sampler2D",
    [spvc::SPIRType::Sampler] = "sampler",
    [spvc::SPIRType::AccelerationStructure] = "AccelerationStructure",
    [spvc::SPIRType::RayQuery] = "RayQuery",
    [spvc::SPIRType::ControlPointArray] = "ControlPointArray",
    [spvc::SPIRType::Interpolant] = "Interpolant",
    [spvc::SPIRType::Char] = "Char",
};

//fread is more general than fstream
static inline std::vector<uint32_t>
read_file(const char *path)
{
    FILE *file = fopen(path, "rb");
    if (!file) {
        std::cerr << "failed to open " << path << std::endl;
        return {};
    }
    fseek(file, 0, SEEK_END);
    size_t len = ftell(file) / sizeof(uint32_t);
    rewind(file);

    std::vector<uint32_t> data(len);
    if (fread(data.data(), sizeof(uint32_t), len, file) != len)
        data.clear();

    fclose(file);
    return data;
}

static void reflect_io(const spvc::SmallVector<spvc::Resource>& vars,
                           spvc::Compiler& comp, std::string name)
{
    std::cout << vars.size() << " stage " << name << std::endl;

    for (const auto& var : vars) {
        auto type = comp.get_type(var.type_id);

        std::cout << "input " << var.name << " has type: " << BTMap[type.basetype];
        std::cout << " at location: " << comp.get_decoration(var.id,
                                                             spv::DecorationLocation);
        std::cout << " of size " << type.vecsize << "x" << type.columns;

        std::cout << " is " << (type.array[0] ) << " an array";

        std::cout << std::endl;
    }
}


int main(int argc, char *argv[])
{
    std::vector<uint32_t> spv_data = std::move(read_file(argv[1]));

    if (spv_data.empty()) {
        std::cerr << "Failed to read spirv code from " << argv[1] << std::endl;
        return EXIT_FAILURE;
    }

    //we need to create reflection source using const uint32_t, using std::move
    //will let it eat the data.
    spvc::CompilerReflection compiler(spv_data.data(), spv_data.size());

    auto resources = compiler.get_shader_resources();
    std::cout << resources.push_constant_buffers.size() << " push constants" << std::endl;
    std::cout << resources.stage_outputs.size() << " stage outputs" << std::endl;
    reflect_io(resources.stage_inputs, compiler, "input");
    reflect_io(resources.stage_outputs, compiler, "output");

    return 0;
}
