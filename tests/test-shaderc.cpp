#include <algorithm>
#include <iostream>
#include <iterator>
#include <memory>
#include <shaderc/shaderc.h>
#include <shaderc/shaderc.hpp>
#include <string>
#include "includer.hpp"


static bool
test_string(const std::string& fname, shaderc_shader_kind kind,
            const std::string code)
{
    shaderc::Compiler compiler;
    shaderc::CompileOptions options;

    options.SetIncluder(std::unique_ptr<FileIncluder>(new FileIncluder));
    options.SetOptimizationLevel(shaderc_optimization_level_zero);

    shaderc::SpvCompilationResult result =
        compiler.CompileGlslToSpv(code, kind, fname.c_str(), options);

    if (result.GetCompilationStatus() != shaderc_compilation_status_success) {
        std::cerr << "failed to compile the shader: " << fname << std::endl;
        std::cerr << "err_msg: " << result.GetErrorMessage() << std::endl;
        return false;
    } else if (result.GetNumWarnings()) {
        std::cerr << "we have warnings: " << result.GetErrorMessage() << std::endl;
        return true;
    }
    return true;
}

static bool
test_preprocess(const std::string& fname, shaderc_shader_kind kind,
                const std::string code)
{
    shaderc::Compiler compiler;
    shaderc::CompileOptions options;

    options.SetIncluder(std::unique_ptr<FileIncluder>(new FileIncluder));
    options.SetOptimizationLevel(shaderc_optimization_level_zero);

    shaderc::PreprocessedSourceCompilationResult result =
        compiler.PreprocessGlsl(code, kind, fname.c_str(), options);
    if (result.GetCompilationStatus() != shaderc_compilation_status_success) {
        std::cerr << "failed to compile the shader: " << fname << std::endl;
        std::cerr << "err_msg: " << result.GetErrorMessage() << std::endl;
        return false;
    } else if (result.GetNumWarnings()) {
        std::cerr << "we have warnings: " << result.GetErrorMessage() << std::endl;
        return true;
    } else {
        std::string processed;
        std::copy(result.begin(), result.end(), std::back_inserter(processed));
        std::cout << "processed:\n";
        std::cout << processed << std::endl;
    }
    return true;
}

int main(int argc, char *argv[])
{
    std::string code =
        "#version 450\n"
        "#include \"common.glsl\"\n"
        "layout(location = 0) in vec3 position;\n"
        "layout(set=0, binding=0) uniform mpv {\n"
        "    mat4 mpv;\n"
        "} gmpv;\n"
        "void main() {\n"
        "    gl_Position = gmpv.mpv * vec4(position, 1.0);\n"
        "}";

    test_string("/tmp/shader.vert", shaderc_glsl_vertex_shader, code);
    test_preprocess("/tmp/shader.vert", shaderc_glsl_vertex_shader, code);

    return 0;
}
