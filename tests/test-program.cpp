#include <cassert>
#include <cstdlib>
#include <pstl/glue_algorithm_defs.h>
#include <spirv.hpp>
#include <spirv_common.hpp>
#include <stdio.h>
#include <stdint.h>

#include <iostream>
#include <iterator>
#include <utility>
#include <iostream>
#include <fstream>
#include <vector>
#include <spirv_cross.hpp>
#include <spirv_reflect.hpp>

#include "ShaderProgram.hpp"


namespace spvc = spirv_cross;

//fread is more general than fstream
static inline std::vector<uint32_t>
read_file(const char *path)
{
    FILE *file = fopen(path, "rb");
    if (!file) {
        std::cerr << "failed to open " << path << std::endl;
        return {};
    }
    fseek(file, 0, SEEK_END);
    size_t len = ftell(file) / sizeof(uint32_t);
    rewind(file);

    std::vector<uint32_t> data(len);
    if (fread(data.data(), sizeof(uint32_t), len, file) != len)
        data.clear();

    fclose(file);
    return data;
}

int main(int argc, char *argv[])
{
    std::vector<std::vector<uint32_t>> shaders;

    shaders.emplace_back(std::move(read_file(argv[1])));
    shaders.emplace_back(std::move(read_file(argv[2])));

    ShaderProgramDynamic program;
    std::cout << "before move: " << shaders[0].size() << std::endl;
    program.AddShaders(std::move(shaders));
    std::cout << "after move: " << shaders[0].size() << std::endl;
}
