#pragma once

#include <string>

//now we have signatures, compare them could be simply as strcmp
enum ShaderTypeSignature
{
    // literal types
    SHADER_BTYPE_BOOL = 'b',
    SHADER_BTYPE_CHAR = 'c',
    SHADER_BTYPE_INT16 = 'n',
    SHADER_BTYPE_UINT16 = 'q',
    SHADER_BTYPE_INT = 'i',
    SHADER_BTYPE_UINT = 'u',
    SHADER_BTYPE_HALF = 'h',
    SHADER_BTYPE_FLOAT = 'f',
    SHADER_BTYPE_DOUBLE = 'd',
    // vector/matrix
    SHADET_BTYPE_VECTOR = 'v', // vec3 = v{3f}, ivec3 = v{3i}
    SHADER_BTYPE_MATRIX = 'm', // mat3 = m{33f}, imat2x3i = m{23i}

    //compond type
    SHADER_CTYPE_ARRAY = 'a', //vec3[] = a{v{3f}}
    SHADER_CTYPE_STRUCT = 's', //complex types

    //uniform opaque types
    SHADER_UTYPE_ATOMIC = 'A',  //atomic_uint
    SHADER_UTYPE_SAMPLER = 'S', //sampler2d = S{2}, samplerCube = S{c},
                                //sampler2DArray = S{2a}, samplerBuffer = S{b}
    SHADER_UTYPE_IMAGE = 'M',   //similar to sampler
    SHADER_UTYPE_TEXTURE = 'T', //similar to sampler
    SHADER_UTYPE_BUFFER = 'B', //buffer, signature similar to struct
    SHADER_UTYPE_ACCLSTRT = 'R' //accelerationStructureEXT used in raytracing
};

//the shader stage reflects to vkShaderStageFlagBits
enum ShaderStage {
    SHADER_STAGE_VERT = 0x1, //vertex shader
    SHADER_STAGE_TESC = 0x2, //tessellation control shader
    SHADER_STAGE_TESE = 0x4, //tessellation evaluation shader
    SHADER_STAGE_GEOM = 0x8, //geometry shader
    SHADER_STAGE_FRAG = 0x10, //fragment shader
    SHADER_STAGE_COMP = 0x20, //compute shader
    SHADER_STAGE_TASK = 0x40, //task shader
    SHADER_STAGE_MESH = 0x80, //mesh shader
    SHADER_STAGE_RGEN = 0x100, //raygen shader
    SHADER_STAGE_RAHIT = 0x200, //ray any hit shader
    SHADER_STAGE_RCHIT = 0x400, //ray closest hit shader
    SHADER_STAGE_RMISS = 0x800, //ray miss shader
    SHADER_STAGE_RINT = 0x1000, //ray intersection shader
    SHADER_STAGE_RCALL = 0x2000, //ray call shader
};


// some examples in glsl
// 1: layout(location = x) in vec3 position -> v{3f}
// 2: in BlockName {
//      ivec3 someIns;
//      vec4  value;
//} -> s
typedef std::string ShaderStageIO[8];

// here we collect a few shaders input output variables

// 1: vertex shader:
// layout (location = x) in  vec3 postion;
// layout (location = y) out vec4 pos;

// 2: fragment shader:
// layout (location = x) in vec3 pos;
// layout (location = y) out vec4 fragColor;
// layout (input_attachment_index = z, binding = i) uniform subpassInput depth;

// 3: tessellation control shader:
// layout (location = x) in vec3 inNormal[]; //so this is an array
// layout (location = y) out vec3 outNormal[3]; //we have a size now?

// 4: tessellation evaluation shader
// layout (location = x) in vec3 inNormal[];
// layout (location = y) out vec3 outNormal[];

// 5: geometry shader
// layout (triangles) in;
// layout (line_strip, max_vertices = 6) out;
// layout (location = x) in vec3 inNormal[];
// layout (location = y) out vec3 outNormal; //use emit vertex

// 6 compute shader
// layout (local_size_x = M, local_size_y = N, local_size_z = P) in;

// 7 raygen shader
// note that raypayload can be a struct.
// layout (location = 0) rayPlayLoad vec3 hitValue;
