// Copyright 2015 The Shaderc Authors. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef GLSLC_FILE_INCLUDER_H_
#define GLSLC_FILE_INCLUDER_H_


#include <filesystem>
#include <mutex>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>
#include <unordered_set>

#include "shaderc/shaderc.hpp"

class FileFinder {
public:
    std::string FindReadableFilePath(const std::string& filename) const;
    std::string FindRelativeFilePath(const std::string& base,
                                     const std::string& filename) const;
    bool AddSearchPath(const std::string& path);

private:
    std::vector<std::filesystem::path> mSearchPaths;
};

// thread-safe
class FileIncluder : public shaderc::CompileOptions::IncluderInterface {
public:
    FileIncluder() = default;
    ~FileIncluder() = default;

    shaderc_include_result* GetInclude(const char* requested_source,
                                       shaderc_include_type type,
                                       const char* requesting_source,
                                       size_t include_depth) override;
    // Releases an include result.
    void ReleaseInclude(shaderc_include_result* include_result) override;

    // Returns a reference to the member storing the set of included files.
    const std::unordered_set<std::string>& FilePathTrace() const {
        return mIncludedFiles;
    }

private:
    // Used by GetInclude() to get the full filepath.
    FileFinder mFileFinder;
    // The full path and content of a source file.
    struct FileInfo {
        const std::string mFullPath;
        std::vector<char> mContents;
    };

    // The set of full paths of included files.
    std::unordered_set<std::string> mIncludedFiles;
};


#endif  // GLSLC_FILE_INCLUDER_H_
