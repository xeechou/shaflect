#pragma once

// shader program
#include <memory>
#include <map>
#include <stdint.h>
#include <vector>
#include <array>

#include "ShaderType.hpp"

//should include program type

//input outputs
struct ProgramInterfaceVariable {
    uint32_t mLocation;
    const char *mName;
    const char *mSiganture;
    //TODO builtin variable, decorations?
};

struct ProgramPushConstant {
    const char *mName;
    const char *mSignature; //enough to describe the type
    uint32_t mOffset;
    uint32_t mAbsOffset;
    uint32_t mSize;
    uint32_t mPaddedSize;
};

//describe the descriptor sets in all shaders
struct ProgramDescriptorBinding {
    const char *name;
    uint32_t binding;

    //you can specify format in binding like:
    //layout (binding = i, set = j, rgba8) uniform image2D image;
    uint32_t fmt;

    // input attachments? they can be passed in like
    // layout (input_attachment_index = z, binding = i) uniform subpassInput depth;

};

//make it into class so we can acess the variables?
struct ProgramDescriptorSet {
    uint32_t mSet;
    uint32_t mNumBindings;
    ProgramDescriptorBinding *mBindings; //or make it into double pointer?

    //TODO it is not possible to directly access the binding like
    //set->bingdingSampler0. What we can do is writing code like
    //set->bindingSampler0() on subclasses
};

struct ProgramShader {
    //TODO: get shader type

    const uint32_t mSize, *mCode;
};

// We do this, then compile time validation is no more. Maybe it is not
// possible to do it

class ShaderProgram {
public:
    enum Type {
	//inital value
        NO_RPOGRAM = 0,
        //maybe a subpass
        FRAGMENT_PROGRAM = SHADER_STAGE_FRAG,
        //traditional rasterization shader
        RASTERIZE_PROGRAM =
        SHADER_STAGE_VERT | SHADER_STAGE_TESC | SHADER_STAGE_TESE |
        SHADER_STAGE_GEOM | SHADER_STAGE_FRAG,
        //new mesh rasterization shader
        MESHNV_PROGRAM =
        SHADER_STAGE_TASK | SHADER_STAGE_MESH | SHADER_STAGE_FRAG,
        //compute program
        COMPUTE_PROGRAM = SHADER_STAGE_COMP,
        //raytrace program
        RAYTRACE_PROGRAM = SHADER_STAGE_RGEN | SHADER_STAGE_RAHIT |
        SHADER_STAGE_RCHIT | SHADER_STAGE_RMISS | SHADER_STAGE_RINT |
        SHADER_STAGE_RCALL,
    };

    //the input and output of program
    uint8_t mNumShaders;
    uint8_t mNumInputs;
    uint8_t mNumOutputs;

    uint8_t mNumDescSets;
    uint8_t mNumPushConsts;
    uint8_t mNumSpecConsts; //vkSpecializationInfo


    //initialize compute shader
    ShaderProgram() : mType(NO_RPOGRAM), mStageFlags(0) {}

    ShaderProgram(uint32_t localX, uint32_t localY, uint32_t localZ) :
        mType(COMPUTE_PROGRAM), mStageFlags(SHADER_STAGE_COMP),
        mLocalSize(localX, localY, localZ) {}

    enum ShaderProgram::Type GetType() const { return mType; }

protected:
    enum Type mType;
    uint32_t mStageFlags;
    //for compute shader
    struct LocalSize {
        uint32_t x, y, z;
        LocalSize() { x = 0; y = 0; z = 0; }
        LocalSize(uint32_t X, uint32_t Y, uint32_t Z) { x = X; y = Y; z = Z; }
    } mLocalSize;
};

template<size_t NS, size_t NI, size_t NO, size_t ND, size_t NP>
class ShaderProgramStatic : public ShaderProgram {
protected:
    std::array<ProgramShader, NS> mShaders;
    std::array<ProgramInterfaceVariable, NI> mInputs;
    std::array<ProgramInterfaceVariable, NO> mOutputs;
    std::array<ProgramDescriptorSet, ND> mDescSets;
    std::array<ProgramPushConstant, NP> mPushConsts;
};

//the non templated version of ShaderProgram, the class shall not have a
//constructor, instead, we take a static method to return a shaderProgram when
//needed
class ShaderProgramDynamic : public ShaderProgram {
public:
    static std::unique_ptr<ShaderProgramDynamic> CreateFromShaders();
    //TODO, move it to protected
    bool AddShaders(std::vector<std::vector<uint32_t>>&& shaders);
protected:

    std::vector<ProgramShader> mShaders;
    std::vector<ProgramInterfaceVariable> mInputs;
    std::vector<ProgramInterfaceVariable> mOutputs;
    std::vector<ProgramDescriptorSet> mDescSets;
    std::vector<ProgramPushConstant> mPushConsts;

    std::map<ShaderStage, std::vector<uint32_t>> mModules;
    //TODO use a map to represent the shaders

};
