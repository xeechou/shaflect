#pragma once

#include <stdint.h>

#if defined(_WIN32)
#include "port/strings.h"
#elif defined (__GNUC__) || defined (__clang__)
#include <strings.h>
#endif


// there are some functions simply just not standard across platform

namespace {

static inline uint8_t FindFirstSet(uint32_t value)
{
    return ffs(value);
}


}
