#include <assert.h>
#include <cstddef>
#include <filesystem>
#include <cstring>
#include <cstdio>
#include <fstream>
#include <ios>
#include <iostream>
#include <streambuf>
#include <string>
#include <vector>

#include "includer.hpp"

namespace fs = std::filesystem;

///////////////////////////////////////////////////////////////////////////////
// file finders
///////////////////////////////////////////////////////////////////////////////

std::string FileFinder::FindReadableFilePath(const std::string& filename) const
{
    assert(!filename.empty());

    for (const auto& dir : mSearchPaths) {
        fs::path filePath = dir / fs::path(filename);

        if (fs::exists(filePath))
            return filePath.string();
    }
    return "";
}

std::string FileFinder::FindRelativeFilePath(const std::string& base,
                                             const std::string& filename) const
{
    assert(!filename.empty());

    fs::path dir = fs::path(base).parent_path();
    fs::path filePath = dir / fs::path(filename);

    if (fs::exists(filePath))
        return filePath.string();
    return FindReadableFilePath(filename);
}

///////////////////////////////////////////////////////////////////////////////
// includer
///////////////////////////////////////////////////////////////////////////////

static shaderc_include_result* MakeErrorIncludeResult(const char* message) {
    return new shaderc_include_result{"", 0, message, ::strlen(message)};
}

static bool ReadFile(std::string fullPath, std::vector<char>& contents)
{
    std::ifstream stream(fullPath, std::ios_base::binary);

    if (!stream.is_open())
        return false;

    stream.seekg(0, std::ios::end);
    size_t size = static_cast<size_t>(stream.tellg());
    stream.seekg(0, std::ios::beg);

    contents.resize(size);
    stream.read(contents.data(), size);
    return true;
}

shaderc_include_result* FileIncluder::GetInclude(
    const char* requested_source, shaderc_include_type include_type,
    const char* requesting_source, size_t) {

  const std::string full_path =
      (include_type == shaderc_include_type_relative)
          ? mFileFinder.FindRelativeFilePath(requesting_source,
                                             requested_source)
          : mFileFinder.FindReadableFilePath(requested_source);

  if (full_path.empty())
    return MakeErrorIncludeResult("Cannot find or open include file.");

  // In principle, several threads could be resolving includes at the same
  // time.  Protect the included_files.

  // Read the file and save its full path and contents into stable addresses.
  FileInfo* new_file_info = new FileInfo{full_path, {}};
  if (!::ReadFile(full_path, new_file_info->mContents)) {
      return MakeErrorIncludeResult("Cannot read file");
  }

  mIncludedFiles.insert(full_path);

  return new shaderc_include_result{
      new_file_info->mFullPath.data(), new_file_info->mFullPath.length(),
      new_file_info->mContents.data(), new_file_info->mContents.size(),
      new_file_info};
}

void FileIncluder::ReleaseInclude(shaderc_include_result* include_result) {
    FileInfo* info = static_cast<FileInfo*>(include_result->user_data);
    delete info;
    delete include_result;
}
