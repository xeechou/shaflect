#include <algorithm>
#include <array>
#include <cassert>
#include <iostream>
#include <map>
#include <pstl/glue_algorithm_defs.h>
#include <spirv.hpp>
#include <stdint.h>
#include <spirv_cross.hpp>
#include <spirv_reflect.hpp>
#include <utility>
#include <vector>

// #include <spirv_reflect.h>
#include "ShaderProgram.hpp"
#include "compatibility.h"

namespace spvc = spirv_cross;
typedef std::vector<uint32_t> ShaderCode;

static const std::array<const char *, 14> StageNameMapper {
    "vertex shader", //vert
    "tessellection control shader", //tssc
    "tessellection evaluatoin shader", //tsse
    "geometry shader", //geo
    "fragment shader", //frag

    "compute shader",   //comp

    "mesh task shader", //task
    "mesh shader", //mesh

    "ray generation shader",  //rgen
    "any hit shader",  //anyh
    "closest hit shader",  //clos
    "miss shader",  //miss
    "ray intersection shader",  //intr
    "ray callable shader",  //call
};

//we have to make sure they map correctly
static const ShaderStage StageMaps[14] = {
    ShaderStage::SHADER_STAGE_VERT,
    ShaderStage::SHADER_STAGE_TESC,
    ShaderStage::SHADER_STAGE_TESE,
    ShaderStage::SHADER_STAGE_GEOM,
    ShaderStage::SHADER_STAGE_FRAG,
    ShaderStage::SHADER_STAGE_COMP,
    ShaderStage::SHADER_STAGE_TASK,  //5267 -> 6
    ShaderStage::SHADER_STAGE_MESH, //5268 -> 7
    //ray trace shader, not mapped corectly
    ShaderStage::SHADER_STAGE_RGEN, //5313 -> 8
    ShaderStage::SHADER_STAGE_RINT, //5314 -> 9
    ShaderStage::SHADER_STAGE_RAHIT, //5135 -> 10
    ShaderStage::SHADER_STAGE_RCHIT, //5316 -> 11
    ShaderStage::SHADER_STAGE_RMISS, //5317 -> 12
    ShaderStage::SHADER_STAGE_RCALL, //5318 -> 13

};

static ShaderStage MapShaderStage(uint32_t model)
{
    if (model <= spv::ExecutionModel::ExecutionModelGLCompute) {
        return StageMaps[model];
    } else if (model >= spv::ExecutionModel::ExecutionModelTaskNV &&
               model <= spv::ExecutionModel::ExecutionModelMeshNV) {
        model = model - spv::ExecutionModel::ExecutionModelTaskNV +
            spv::ExecutionModelKernel;
        return StageMaps[model];
    } else { //must be ray trace shader
        model = model - spv::ExecutionModelRayGenerationKHR + 8;
        assert(model >= 8);
        return StageMaps[model];
    }
}

static ShaderProgram::Type GuessProgramType(uint32_t stageFlags)
{
    static const enum ShaderProgram::Type types[] = {
        ShaderProgram::FRAGMENT_PROGRAM,
        ShaderProgram::RASTERIZE_PROGRAM,
        ShaderProgram::MESHNV_PROGRAM,
        ShaderProgram::COMPUTE_PROGRAM,
        ShaderProgram::RAYTRACE_PROGRAM
    };
    //we can go from no program to any type; fragment program to a rasterize
    //program or mesh program. Otherwise we are locked to the program type now.
    for (auto type : types) {
        if ((stageFlags & type) == stageFlags)
            return type;
    }
    return ShaderProgram::NO_RPOGRAM;
}

static ShaderProgram::Type CheckAddShader(enum ShaderStage stage,
                                          uint32_t stages)
{
    const char *name = StageNameMapper[::FindFirstSet(stage)];

    if ((stage | stages) == stages) {
        std::cerr << "Shader module " << name << " already added" << std::endl;
        return ShaderProgram::NO_RPOGRAM;
    }
    return GuessProgramType(stage | stages);
}

//check input-output mapping and generate descriptor map
static bool CheckGraphicsPrograms(
    const std::map<ShaderStage, ShaderCode>& modules,
    uint32_t stages)
{
    auto vert = modules.find(ShaderStage::SHADER_STAGE_VERT);
    auto frag = modules.find(ShaderStage::SHADER_STAGE_FRAG);
    auto tesc = modules.find(ShaderStage::SHADER_STAGE_TESC);
    auto tese = modules.find(ShaderStage::SHADER_STAGE_TESE);

    if (!(stages & SHADER_STAGE_VERT))
        return false;

    if (vert != modules.end()) {
        spvc::CompilerReflection c(vert->second.data(), vert->second.size());


    }

    return true;
}

bool ShaderProgramDynamic::AddShaders(std::vector<ShaderCode> &&shaders)
{
    ShaderProgram::Type type = NO_RPOGRAM;
    uint32_t stages = 0;

    if (mType != NO_RPOGRAM)
        return false;

    //check if no conflicts in shader stages
    for (auto& shader : shaders) {
        //this is really expensive,
        spvc::CompilerReflection comp(shader.data(), shader.size());
        auto entries = comp.get_entry_points_and_stages();
        assert(entries.size() == 1);

        //the stage is mapped to vkShaderStageFlag
        ShaderStage stage = MapShaderStage(entries[0].execution_model);
        auto t = CheckAddShader(stage, stages);
        if (t == ShaderProgram::NO_RPOGRAM) {
            return false;
        }
        stages |= stage;
        type = t;

        //now move the stages
        mModules[stage] = std::move(shader);
    }

    // mModules = std::move(shaders);
    mType = type;
    mStageFlags = stages;
    return true;
}
