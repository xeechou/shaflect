## Shaflect

small library for spirv shader compilation and reflection. shaders can be
compiled into header and source files with exposed resources
interface. External project will link to this library to avoid loading spirv
shader at runtime.
